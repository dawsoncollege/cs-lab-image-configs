# cs-lab-image

__Public__ repository in which we can share configuration files with IT for preparing our Windows image for computer labs, laptops, podiums, etc.

The files in this repository are meant to be linked to from the annual Lab Master document shared with IT. 

## Contributing

1. Please work in a feature branch and open an MR to make changes.
2. Include the __course number__ in the MR title if the changes relate to a specific course.
3. Flag __one co-teacher__ (someone who is familiar with the course that your change relates to) and __one member of the CS-IT committee for review__.
4. Merge your own MR once it's approved.

